import paho.mqtt.client as mqtt
import requests



# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
#client.loop_forever()

class MqttController:
    def __init__(self):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.connect("guldimir.cambiadeso.es", 1883, 60)
        
        
    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
#        client.subscribe("$SYS/#")
        client.subscribe("palabrita")        
        client.subscribe("custom")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        if msg.topic == "palabrita":
            self.random_palabrita(msg.payload)
        if msg.topic == "custom":
            self.custom_palabrita(msg.payload)


    def loop_forever(self):
        self.client.loop_forever()
        

    def random_palabrita(self, msgpayload):
        r = requests.get("http://localhost:8888/random")
        print(r)
        print(r.status_code)
        self.client.publish("status", "Random " + str(r.status_code))


    def custom_palabrita(self, msgpayload):
        r = requests.get("http://localhost:8888/custom?texto=hola&cita=mundo")
        print(r)
        print(r.status_code)
        self.client.publish("status", "Custom " + str(r.status_code))
            
if __name__ == "__main__":
    client = MqttController()
    client.loop_forever()    
