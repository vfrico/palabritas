from flask import Flask
from flask import request
from gpiozero import Button
import genera_palabra

app = Flask(__name__)

palabritas = []

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


def push_new_random():
    new_palabrita = genera_palabra.gen_random()
    palabritas.append(new_palabrita)

    
@app.route("/random")
def random_palabrita():
    try:
        palabra_img = palabritas.pop()
    except IndexError:
        palabra_img = genera_palabra.gen_random()
        
    genera_palabra.print_palabrita(palabra_img)

    push_new_random()
    return "Palabra generated."


@app.route("/custom")
def custom_palabrita():
    texto = request.args.get("texto")
    cita = request.args.get("cita")
    custom_img = genera_palabra.gen_random_style(texto, cita)
    result = genera_palabra.print_palabrita(custom_img)
    return "Custom palabra generada. Result was:" + str(result)
    

def init_hw():
    print("init hw")

    def say_hello():
        print("Hello!")

    def say_goodbye():
        print("Goodbye")

    button = Button(2)
    button.when_pressed = say_hello
    #button.when_released = palabrita
    button.when_released = say_goodbye


if __name__ == "__main__":
#    init_hw()
    app.run(host="0.0.0.0", port="8888")
