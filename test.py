import os
import random
import time
import genera_palabra


citas = genera_palabra.citas

lista_fuentes = [ 
{"id": "ta_000", "src": "Alarate.ttf", "v_pad": lambda x: 0},
{"id": "ta_002", "src": "Ameliya.ttf", "v_pad": lambda x: 0},
{"id": "ta_003", "src": "Amigo.ttf", "v_pad": lambda x: 0},
{"id": "ta_004", "src": "Amiya.ttf", "v_pad": lambda x: 0},
{"id": "ta_005", "src": "Astria.ttf", "v_pad": lambda x: 0},
{"id": "ta_006", "src": "Austin.ttf", "v_pad": lambda x: 0},
{"id": "ta_007", "src": "Ayahi.ttf", "v_pad": lambda x: 0},
{"id": "ta_009", "src": "Bahagia.ttf", "v_pad": lambda x: 0},
{"id": "ta_010", "src": "Bahamia.ttf", "v_pad": lambda x: 0},
{"id": "ta_013", "src": "Beautiful Days.ttf", "v_pad": lambda x: 0},
{"id": "ta_014", "src": "Belinda.ttf", "v_pad": lambda x: 0},
{"id": "ta_015", "src": "Belly Molly.ttf", "v_pad": lambda x: 0},
{"id": "ta_016", "src": "Bestina.ttf", "v_pad": lambda x: 0},
{"id": "ta_018", "src": "Blastinae.ttf", "v_pad": lambda x: 0},
{"id": "ta_024", "src": "Bolyna.ttf", "v_pad": lambda x: 0},
{"id": "ta_025", "src": "Boxes Dream.ttf", "v_pad": lambda x: 0},
{"id": "ta_026", "src": "Brightlast.ttf", "v_pad": lambda x: 0},
{"id": "ta_031", "src": "Bythemis Quertas.ttf", "v_pad": lambda x: 0},
{"id": "ta_032", "src": "California Sunrise.ttf", "v_pad": lambda x: 0},
{"id": "ta_034", "src": "Charllie.ttf", "v_pad": lambda x: 0},
{"id": "ta_035", "src": "Choges.ttf", "v_pad": lambda x: 0},
{"id": "ta_039", "src": "Elevena.ttf", "v_pad": lambda x: 0},
{"id": "ta_040", "src": "Emelian.ttf", "v_pad": lambda x: 0},
{"id": "ta_041", "src": "Endah.ttf", "v_pad": lambda x: 0},
{"id": "ta_042", "src": "Fashion.ttf", "v_pad": lambda x: 0},
{"id": "ta_043", "src": "Fiallio.ttf", "v_pad": lambda x: 0},
{"id": "ta_045", "src": "Follio Cooper.ttf", "v_pad": lambda x: 0},
{"id": "ta_048", "src": "Frayhord.ttf", "v_pad": lambda x: 0},
{"id": "ta_049", "src": "Friday.ttf", "v_pad": lambda x: 0},
{"id": "ta_050", "src": "Gashyna.ttf", "v_pad": lambda x: 0},
{"id": "ta_053", "src": "Haifun.ttf", "v_pad": lambda x: 0},
{"id": "ta_055", "src": "Havana.ttf", "v_pad": lambda x: 0},
{"id": "ta_056", "src": "HellaMella.ttf", "v_pad": lambda x: 0},
{"id": "ta_058", "src": "Heybing.ttf", "v_pad": lambda x: 0},
{"id": "ta_062", "src": "Hisyam.ttf", "v_pad": lambda x: 0},
{"id": "ta_064", "src": "Hummingbird.ttf", "v_pad": lambda x: 0},
{"id": "ta_066", "src": "Innocent Girl.ttf", "v_pad": lambda x: 0},
{"id": "ta_067", "src": "Jellia.ttf", "v_pad": lambda x: 0},
{"id": "ta_073", "src": "Lomigutta.ttf", "v_pad": lambda x: 0},
{"id": "ta_074", "src": "Lonely.ttf", "v_pad": lambda x: 0},
{"id": "ta_075", "src": "Los Angeles.ttf", "v_pad": lambda x: 0},
{"id": "ta_076", "src": "Loveline.ttf", "v_pad": lambda x: 0},
{"id": "ta_077", "src": "Madeira.ttf", "v_pad": lambda x: 0},
{"id": "ta_078", "src": "Mahisya.ttf", "v_pad": lambda x: 0},
{"id": "ta_079", "src": "Makgraf.ttf", "v_pad": lambda x: 0},
{"id": "ta_080", "src": "Mallaba.ttf", "v_pad": lambda x: 0},
{"id": "ta_083", "src": "Mayham.ttf", "v_pad": lambda x: 0},
{"id": "ta_086", "src": "Milya.ttf", "v_pad": lambda x: 0},
{"id": "ta_087", "src": "Minista.ttf", "v_pad": lambda x: 0},
{"id": "ta_088", "src": "Minoila.ttf", "v_pad": lambda x: 0},
{"id": "ta_090", "src": "Missing.ttf", "v_pad": lambda x: 0},
{"id": "ta_093", "src": "Mommy.ttf", "v_pad": lambda x: 0},
{"id": "ta_094", "src": "MonthellaRegular.ttf", "v_pad": lambda x: 0},
{"id": "ta_095", "src": "Mosgrade.ttf", "v_pad": lambda x: 0},
{"id": "ta_101", "src": "Muriely.ttf", "v_pad": lambda x: 0},
{"id": "ta_102", "src": "Notograph Bleed.ttf", "v_pad": lambda x: 0},
{"id": "ta_104", "src": "Omellia.ttf", "v_pad": lambda x: 0},
{"id": "ta_105", "src": "Originality Script.ttf", "v_pad": lambda x: 0},
{"id": "ta_107", "src": "Quebec.ttf", "v_pad": lambda x: 0},
{"id": "ta_110", "src": "Range Orange.ttf", "v_pad": lambda x: 0},
{"id": "ta_111", "src": "Rattaullie.ttf", "v_pad": lambda x: 0},
{"id": "ta_112", "src": "Redstone.ttf", "v_pad": lambda x: 0},
{"id": "ta_114", "src": "Revolage Script Oblique.ttf", "v_pad": lambda x: 0},
{"id": "ta_119", "src": "Reyhan.ttf", "v_pad": lambda x: 0},
{"id": "ta_126", "src": "Rosalinda.ttf", "v_pad": lambda x: 0},
{"id": "ta_130", "src": "Sahiya.ttf", "v_pad": lambda x: 0},
{"id": "ta_131", "src": "Santiago.ttf", "v_pad": lambda x: 0},
{"id": "ta_133", "src": "Shuttezerg.ttf", "v_pad": lambda x: 0},
{"id": "ta_134", "src": "Silence.ttf", "v_pad": lambda x: 0},
{"id": "ta_141", "src": "Summer Tropical.ttf", "v_pad": lambda x: 0},
{"id": "ta_142", "src": "Sunday.ttf", "v_pad": lambda x: 0},
{"id": "ta_143", "src": "Super Wonder.ttf", "v_pad": lambda x: 0},
{"id": "ta_146", "src": "Sweet Duck.ttf", "v_pad": lambda x: 0},
{"id": "ta_147", "src": "Tails Mermaid.ttf", "v_pad": lambda x: 0},
{"id": "ta_149", "src": "The Hartes.ttf", "v_pad": lambda x: 0},
{"id": "ta_150", "src": "The Heglio.ttf", "v_pad": lambda x: 0},
{"id": "ta_151", "src": "The Matesih.ttf", "v_pad": lambda x: 0},
{"id": "ta_153", "src": "Theyriad.ttf", "v_pad": lambda x: 0},
{"id": "ta_154", "src": "Thinkers.ttf", "v_pad": lambda x: 0},
{"id": "ta_155", "src": "Tilmans.ttf", "v_pad": lambda x: 0},
{"id": "ta_159", "src": "Xiolien.ttf", "v_pad": lambda x: 0},
{"id": "ta_160", "src": "Youth Cow.ttf", "v_pad": lambda x: 0},
{"id": "tb_000", "src": "Astro Legend.otf", "v_pad": lambda x: 0},
{"id": "tb_001", "src": "Breakout.ttf", "v_pad": lambda x: 0},
{"id": "tb_002", "src": "Mallikha Brush.ttf", "v_pad": lambda x: 0},
{"id": "tb_003", "src": "Manhattan Brush Script.ttf", "v_pad": lambda x: 0},
{"id": "tb_004", "src": "MBF Bad Motor.ttf", "v_pad": lambda x: 0},
{"id": "tb_005", "src": "MBF Little Tooth.ttf", "v_pad": lambda x: 0},
{"id": "tb_006", "src": "MBF Louna.ttf", "v_pad": lambda x: 0},
{"id": "tb_007", "src": "MBF Neutral Jack Light.ttf", "v_pad": lambda x: 0},
{"id": "tb_008", "src": "MBF Origin.ttf", "v_pad": lambda x: 0},
{"id": "tb_009", "src": "MBF Silver Night.ttf", "v_pad": lambda x: 0},
{"id": "tb_010", "src": "MBF Thale.ttf", "v_pad": lambda x: 0},
{"id": "tb_011", "src": "MBF Typerisme.ttf", "v_pad": lambda x: 0},
{"id": "tb_012", "src": "Ranille.otf", "v_pad": lambda x: 0},
{"id": "tb_013", "src": "Rantika.ttf", "v_pad": lambda x: 0},
{"id": "tb_014", "src": "Romelio Handwriting.ttf", "v_pad": lambda x: 0},
{"id": "tb_015", "src": "Romelio Sans.ttf", "v_pad": lambda x: 0},
{"id": "tb_016", "src": "Sallomae.ttf", "v_pad": lambda x: 0},
{"id": "tb_017", "src": "Signlode.ttf", "v_pad": lambda x: 0},
{"id": "tb_018", "src": "Smiley Hamster.ttf", "v_pad": lambda x: 0},
{"id": "tb_019", "src": "Wedusa.ttf", "v_pad": lambda x: 0},
{"id": "tb_020", "src": "Carafia.otf", "v_pad": lambda x: 0},
{"id": "tb_021", "src": "Givani.otf", "v_pad": lambda x: 0},
{"id": "tb_022", "src": "Qulio.otf", "v_pad": lambda x: 0},
{"id": "tb_023", "src": "Rolih.otf", "v_pad": lambda x: 0},
{"id": "tb_024", "src": "Sadila.otf", "v_pad": lambda x: 0},
{"id": "tb_025", "src": "Sila.otf", "v_pad": lambda x: 0},
{"id": "tb_026", "src": "Syailendra.ttf", "v_pad": lambda x: 0},
{"id": "tc_2", "src": "Charlebury Script Font.ttf", "v_pad": lambda x: 0},
{"id": "tc_6", "src": "Forever in Love Script Font.ttf", "v_pad": lambda x: 0},
{"id": "td_003", "src":"Banda Niera Medium Italic.ttf", "v_pad": lambda x:0},
{"id": "td_006", "src":"Black Ravens.ttf", "v_pad": lambda x:0},
{"id": "td_007", "src":"Bragley.ttf", "v_pad": lambda x:0},
{"id": "td_008", "src":"Broken Crush.ttf", "v_pad": lambda x:0},
{"id": "td_009", "src":"Bronela Bold.ttf", "v_pad": lambda x:0},
{"id": "td_010", "src":"Bronela Regular.ttf", "v_pad": lambda x:0},
{"id": "td_022", "src":"Burgery.ttf", "v_pad": lambda x:0},
{"id": "td_058", "src":"Hargalia.ttf", "v_pad": lambda x:0},
{"id": "td_059", "src":"Have Heart One.otf", "v_pad": lambda x:0},
{"id": "td_065", "src":"Humeira Bold.ttf", "v_pad": lambda x:0},
{"id": "td_067", "src":"Katenila.ttf", "v_pad": lambda x:0},
{"id": "td_072", "src":"Lovatine.ttf", "v_pad": lambda x:0},
{"id": "td_076", "src":"Manhattan Brush Script.otf", "v_pad": lambda x:0},
{"id": "td_078", "src":"Marviona.ttf", "v_pad": lambda x:0},
{"id": "td_080", "src":"Mayhena Inky.ttf", "v_pad": lambda x:0},
{"id": "td_082", "src":"Meliana Script.ttf", "v_pad": lambda x:0},
{"id": "td_083", "src":"Mellynda.ttf", "v_pad": lambda x:0},
{"id": "td_088", "src":"Montheim.ttf", "v_pad": lambda x:0},
{"id": "td_090", "src":"Mourich Bold.ttf", "v_pad": lambda x:0},
{"id": "td_091", "src":"Mourich.ttf", "v_pad": lambda x:0},
{"id": "td_093", "src":"Nagietha.ttf", "v_pad": lambda x:0},
{"id": "td_100", "src":"Niquitta Mirzani Script.ttf", "v_pad": lambda x:0},
{"id": "td_106", "src":"Rantika.ttf", "v_pad": lambda x:0},
{"id": "td_116", "src":"Revans SemiBold.ttf", "v_pad": lambda x:0},
{"id": "td_119", "src":"Romelio Handwriting.ttf", "v_pad": lambda x:0},
{"id": "td_121", "src":"Rompies.ttf", "v_pad": lambda x:0},
{"id": "td_122", "src":"Royaland Clean.otf", "v_pad": lambda x:0},
{"id": "td_123", "src":"Rusthack.ttf", "v_pad": lambda x:0},
{"id": "td_124", "src":"Sallomae.ttf", "v_pad": lambda x:0},
{"id": "td_134", "src":"Sharely.ttf", "v_pad": lambda x:0},
{"id": "td_136", "src":"Shinoya.ttf", "v_pad": lambda x:0},
{"id": "td_138", "src":"The Brande and Lotaline.ttf", "v_pad": lambda x:0},
{"id": "td_140", "src":"Trashbone.ttf", "v_pad": lambda x:0},
{"id": "td_144", "src":"Vankours.ttf", "v_pad": lambda x:0},
{"id": "te_0", "src":"NeverMindHand-Medium.ttf", "v_pad": lambda x:0},
{"id": "te_1", "src":"NeverMindHandwriting-Regular.ttf", "v_pad": lambda x:0},
{"id": "te_2", "src":"AutourOne-Regular.otf", "v_pad": lambda x:0}, 
{"id": "te_3", "src":"Bainsley_Roman.otf", "v_pad": lambda x:0},
{"id": "te_4", "src":"Caroni-Regular.otf", "v_pad": lambda x:0},
{"id": "te_5", "src":"MoonlessSC-Regular.otf", "v_pad": lambda x:0},
{"id": "te_6", "src":"Sovba_Regular.ttf", "v_pad": lambda x:0}, 
{"id": "te_7", "src":"tillana-medium.ttf", "v_pad": lambda x:0},
]


lista_fuentes = genera_palabra.lista_fuentes

def gen_random_style(texto_cita, ref_cita, font_config):

    bases = ["base-1_rcce.png"]
    base = os.path.join("bases/", bases[0])
    
    fuente = os.path.join("fuentes/", font_config["src"])

    path_img = genera_palabra.generate_imagen(texto_cita, ref_cita, base, fuente, font_config)
    return path_img

def main():
    # texto = "El Señor es mi \"pastor\", «nada me faltará»"
    # texto = "El que escucha y no pone en práctica se parece a uno que edificó una casa sobre tierra, sin cimientos; arremetió contra ella el río, y enseguida se derrumbó desplomándose, y fue grande la ruina de aquella casa"

    start = time.time()
    for font_config in lista_fuentes:
        print(font_config)

        cita = random.choice(citas)
        texto = cita["texto"]

        gen_random_style(texto, f"font: {font_config['id']}", font_config)
        
    end = time.time()

    print("TOTAL", end-start)
if __name__ == "__main__":
    main()