import os
import sys
import time
import math
import random
import textwrap

from PIL import Image, ImageDraw, ImageFont

from escpos import *

from citas import citas
from tipografias import get_random_typography


def gen_text(cols):
    # La m, la s y la o son tres letras que habitualmente ocupan mucho en las tipografías y puede ser usada para estimar el ancho máximo de caracteres
    return "".join(["mios"]*math.ceil(cols/4))

def loop_increment(total_loops, default=2, multiplier=1):
    if total_loops <= 0:
        total_loops = 1
    
    increment = max(default, math.ceil(math.log(total_loops*2)))
    multiplier = max(1, multiplier)

    return math.ceil(increment * multiplier)

def find_best_colwrap(draw: ImageDraw, font: ImageFont, max_width, margin=70, max_cols=15):
    width = 0
    total_loops = 0
   
    # Pre-guess a correct value of max_cols and then check it on the loop below
    width, _h = draw.textsize(gen_text(max_cols), font=font)
    max_cols = math.ceil((max_width - margin*2) / (width/max_cols))

    while width + margin*1.8 < max_width:
        width, _h = draw.textsize(gen_text(max_cols), font=font)
        print(total_loops, width, max_width, max_cols)
        max_cols += 2
        total_loops += 1

    return max_cols - 2


def get_total_height(parrafo, draw: ImageDraw, font: ImageFont):
    total_height = 0
    for line in parrafo:
        _w, height = draw.textsize(line, font=font)
        total_height += height
    return total_height


variant = "_rcce"
# variant = "_jmj"
# Bases grandes
lista_bases_l = [f"base-1.0{variant}.png", f"base-1.1{variant}.png", f"base-1.2{variant}.png"]
# Bases medianas
lista_bases_m = [f"base-0.0{variant}.png", f"base-0.1{variant}.png", f"base-0.2{variant}.png"]


fuente_cita = os.path.join("fuentes/", "AlegreyaSans-Medium.ttf")

img_cache = ""


def print_palabrita(palabrita_img):
    p = printer.Usb(idVendor=0x0416, idProduct=0x5011, timeout=0, in_ep=0x81, out_ep=0x03)

    # para que el usuario tenga feedback del botón
    p.text("\n")

    print("start image print")
    p.image(palabrita_img)
    print("end image print")
    p.cut()

    p.close()


def gen_random_style(texto_cita, ref_cita):
    if len(texto_cita) > 100:
        bases = lista_bases_l
    else:
        bases = lista_bases_m
    base = os.path.join("bases/", random.choice(bases))

    fuente, font_config = get_random_typography()

    path_img = generate_imagen(texto_cita, ref_cita, base, fuente, font_config)
    return path_img


def gen_random():
    cita = random.choice(citas)
    #cita = citas[9]
    texto_cita = cita["texto"]
    ref_cita = cita["cita"]
    
    return gen_random_style(texto_cita, ref_cita)



def generate_imagen(texto_cita, ref_cita, base, fuente, font_config):
    img = Image.open(base)

    # Para poner el fondo blanco:
    img = Image.alpha_composite(Image.new('RGBA', img.size, (255,255,255)), img)
                                
    MAX_W, MAX_H = img.size

    d = ImageDraw.Draw(img)

    font_size = 25
    total_height = 0
    parrafo = []
    
    total_loops = 0

    while total_height + font_size*2 + 30 < MAX_H:
        print()
        print()
        # Probamos por fuerza bruta a ver que el texto no se haga demasiado grande...
        fnt = ImageFont.truetype(fuente, font_size)
        max_cols = find_best_colwrap(d, fnt, MAX_W)
        print("Found max_cols value", max_cols, MAX_W)
        
        parrafo = textwrap.wrap(texto_cita, width=max_cols)

        total_height = get_total_height(parrafo, d, fnt)
        print("fontsize:", font_size, ", total_h", total_height, ", max_h", MAX_H, ", lines: ", len(parrafo), len(texto_cita))

        font_size += loop_increment(total_loops, default=2, multiplier=max_cols/len(texto_cita))#(max_cols/len(texto_cita)) / (total_height/font_size/len(parrafo)))

        total_loops += 1
    print("total de vueltas", total_loops)


    current_h = (MAX_H - total_height) / 3
    v_pad = font_config["v_pad"](font_size)
    for line in parrafo:
        w, h = d.textsize(line, font=fnt)
        d.text(((MAX_W - w) / 2, current_h), line, font=fnt, fill=(0, 0, 0))
        current_h += h + v_pad

    # Texto lugar biblia de la cita
    font_cita = ImageFont.truetype(fuente_cita, 30)
    d.text((20,330), ref_cita, font=font_cita, fill=(0, 0, 0))


    img = img.rotate(90, expand=True)
    #img.show()

    image_saved = "generadas/palabrita_" + str(int(time.time()*10)) + ".png"

    img.save(image_saved)
    return image_saved


def gen_and_print():
    img = gen_random()
    print_palabrita(img)


if __name__ == "__main__":
    gen_and_print()


