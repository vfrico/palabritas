from gpiozero import Button 
from signal import pause 
import time
import requests
import mqttcontroller

def say_hello(): 
    print("Hello!") 

def say_goodbye(): 
    print("Goodbye!") 
    r = requests.get("http://localhost:8888/random")

def mqtt_client():
    trials = 0
    while True:
        try:
            client = mqttcontroller.MqttController()
            client.loop_forever()
        except:
            print("Error trying to connect. Retrying later... Trials: " + str(trials))
            trials += 1
            time.sleep(trials*15) 

def main():
    button = Button(2) 
    button.when_pressed = say_hello 
    button.when_released = say_goodbye 

    print("hello world")

    mqtt_client()
    pause()

    
    
if __name__ == "__main__":
    main()
